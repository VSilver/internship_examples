package com.example;

import com.example.entities.Employee;
import com.example.util.EmployeeDAOFactory;
import com.example.util.HibernateUtil;

public class Main {

  public static void main(String[] args) {
    EmployeeDAOFactory employeeDAOFactory = EmployeeDAOFactory.getInstance();
    
    Employee employee = new Employee("Alex", "Bob", 123456.);
    employeeDAOFactory.getEmployeeDAO().create(employee);
    
    Employee employee2 = employeeDAOFactory.getEmployeeDAO().getOne(employee.getId());
    System.out.println(employee2);
    
    HibernateUtil.getSessionFactory().close();
  }

}
