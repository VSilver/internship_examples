package com.example.util;

import com.example.hybernate_example.EmployeeDAO;
import com.example.hybernate_example.EmployeeDAOImpl;

public class EmployeeDAOFactory {

  private static EmployeeDAOFactory instance;
  private static EmployeeDAO employeeDAO;
  
  private EmployeeDAOFactory() {}
  
  public static synchronized EmployeeDAOFactory getInstance() {
    if (null == instance)
      instance = new EmployeeDAOFactory();
    return instance;
  }

  public static synchronized EmployeeDAO getEmployeeDAO() {
    if (null == employeeDAO)
      employeeDAO = new EmployeeDAOImpl();
    return employeeDAO;
  }
}
