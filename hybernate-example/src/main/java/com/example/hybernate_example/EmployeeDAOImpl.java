package com.example.hybernate_example;

import java.util.List;

import org.hibernate.Session;

import com.example.entities.Employee;
import com.example.util.HibernateUtil;

public class EmployeeDAOImpl implements EmployeeDAO {

  private Session getSession() {
    return HibernateUtil.getSessionFactory().openSession();
  }

  public void create(Employee employee) {
    Session session = null;
    try {
      session = getSession();
      session.beginTransaction();
      session.save(employee);
      session.getTransaction().commit();
    } finally {
      if (session != null && session.isOpen())
        session.close();
    }
  }

  public Employee getOne(Long id) {
    Session session = null;
    Employee employee = null;
    try {
      session = getSession();
      employee = session.get(Employee.class, id);
    } catch (Throwable ex) {
      System.err.println("[Error] getOne exception " + ex);
    } finally {
      if (session != null && session.isOpen())
        session.close();
    }
    return employee;
  }

  public List<Employee> getAll() {
//     List<Employee> employees = getSession().createCriteria(Employee.class).list();
    List<Employee> employees = getSession().createQuery("FROM Employee").getResultList();
    return employees;
  }

  public void update(Employee employee) {
    Session session = getSession();
    session.beginTransaction();
    session.update(employee);
    session.getTransaction().commit();
    session.close();
  }

  public void delete(Long id) {
    delete(getOne(id));
  }

  public void delete(Employee employee) {
    Session session = getSession();
    session.beginTransaction();
    session.delete(employee);
    session.getTransaction().commit();
    session.close();
  }

}
