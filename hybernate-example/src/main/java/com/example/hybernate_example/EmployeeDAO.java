package com.example.hybernate_example;

import java.util.List;

import com.example.entities.Employee;

public interface EmployeeDAO {
   
  void create(Employee employee);
  
  Employee getOne(Long id);
  
  List<Employee> getAll();
  
  void update(Employee employee);
  
  void delete(Long id);
  
  void delete(Employee employee);
  
}