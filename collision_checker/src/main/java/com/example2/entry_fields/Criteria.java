package com.example2.entry_fields;

/**
 * Created by vturcanu on 3/3/2017.
 */
public interface Criteria {
    boolean conflicts(Criteria criteria);
}
