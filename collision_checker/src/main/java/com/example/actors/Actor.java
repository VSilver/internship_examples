package com.example.actors;

import com.example.entry_fields.Rule;

public abstract class Actor {
	public abstract boolean match(Rule rule);
}
