package com.example.entry_fields;

public class Checkbox extends Criteria{
	private Boolean checked;
	
	public Checkbox(boolean checked){
		this.checked = checked;
	}
	
	public Checkbox(){
		this.checked = null;
	}
	
	@Override
	public boolean conflicts(Criteria cr){
		if (!(cr instanceof Checkbox)){
			throw new ClassCastException();
		}
		
		Checkbox chb = (Checkbox) cr;
		
		if (this.checked == null || chb.isChecked() == null){
			return true;
		}
		
		return checked == chb.isChecked();
	}

	public Boolean isChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
}
