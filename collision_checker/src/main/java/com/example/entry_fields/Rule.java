package com.example.entry_fields;

import java.util.HashMap;

import com.example.actors.Person;

public class Rule {
	private static int ruleCounter = 0;
	private int idNumber = 0;
	private HashMap<String, Criteria> criterias;
	
	public Rule(HashMap<String, Criteria> criterias){
		this.criterias = criterias;
	}
	
	public Rule(){
		this.idNumber = ruleCounter++;
		this.criterias = new HashMap<String, Criteria>();
	}
	
	public int getIdNumber(){
		return this.idNumber;
	}
	
	public HashMap<String, Criteria> getCriterias(){
		return criterias;
	}
	
	public void addCriteria(String key, Criteria value){
		this.criterias.put(key, value);
	}

	public Criteria getCriteria(String key) {
		return this.criterias.get(key);
	}
	
	public boolean conflicts(Rule rule){
		HashMap<String, Criteria> otherCriterias = rule.getCriterias();
		
		for (String key : criterias.keySet()){
			Criteria otherCr = otherCriterias.get(key);
			if (!criterias.get(key).conflicts(otherCr)) {
//				System.out.println("" + (this.idNumber + 1) + " doesn't collide with " + (rule.getIdNumber() + 1) + " on " + key);
				return false;
			} else {
//				System.out.println("" + (this.idNumber + 1) + " collides with " + (rule.getIdNumber() + 1) + " on " + key);
			}
		}
		return true;
	}
	
	public boolean match(Person person){
		return true;
	}
}
