package com.example.entry_fields;

public abstract class Criteria {
	public abstract boolean conflicts(Criteria cr) throws ClassCastException;
}
