package com.example.entry_fields;

public class Dropdown<T> extends Criteria{
	
	private T value;
	
	public Dropdown(T value){
		this.value = value;
	}
	
	public Dropdown(){
		this.value = null;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public boolean conflicts(Criteria cr) {
		if (!(cr instanceof Dropdown)){
			throw new ClassCastException();
		}
		Dropdown<T> other = (Dropdown<T>) cr;
		
		if(this.value == null || other.getValue() == null){
			return true;
		}
		
		return other.getValue() == this.value;
	}
}
