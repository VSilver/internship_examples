package com.example.entry_fields;

import javax.management.InvalidAttributeValueException;

public class Range extends Criteria{
	private int min;
	private int max;
	
	public Range(){
		min = 0;
		max = Integer.MAX_VALUE;
	}
	
	public Range(int min, int max) throws InvalidAttributeValueException{
		if (min < max){
			this.min = min;
			this.max = max;
		} else {
			throw new InvalidAttributeValueException();
		}
	}
	
	@Override
	public boolean conflicts(Criteria cr){
		if (!(cr instanceof Range)){
			throw new ClassCastException();
		}
		Range other = (Range) cr;
		return (this.min <= other.getMax() && this.min >= other.getMin() ||
				this.max >= other.getMin() && this.max <= other.getMax());
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		if (min >= 0 && min <= this.max) {
			this.min = min;
		}
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		if (max >= this.min) {
			this.max = max;
		}
	}

}
