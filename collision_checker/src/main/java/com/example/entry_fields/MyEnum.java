package com.example.entry_fields;

/**
 * Created by vturcanu on 3/6/2017.
 */
public enum MyEnum {

    FIRST(""), SECOND("");

    String s;

    MyEnum(String s) {
        this.s = s;
    }
}
