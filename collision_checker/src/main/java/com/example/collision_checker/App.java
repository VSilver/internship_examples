package com.example.collision_checker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.example.entry_fields.*;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main( String[] args )
	{

		List<Rule> rules = populateRuleList();
		
		HashMap<Integer, List<Integer>> collisions = detectCollisions(rules);
		
		if (collisions.isEmpty()){
			System.out.println("There are no collisions.");
		} else {
			for (Integer key : collisions.keySet()){
				String s = "Rule " + (key + 1) + " conflicts with: ";
				for (Integer rule : collisions.get(key)) {
					s += (rule + 1) + " ";
				}
				System.out.println(s);
			}
		}
	}

	public static List<Rule> populateRuleList() {
		List<Rule> rules = new ArrayList<Rule>();
		
		Rule rule = new Rule();
		Range age = new Range();
		age.setMin(20);
		age.setMax(30);
		rule.addCriteria("Age", age);
		rule.addCriteria("Sex", new Dropdown<>(SexValues.M));
		rule.addCriteria("Disabled", new Checkbox(true));
		rules.add(rule);
		
		rule = new Rule();
		age = new Range();
		rule.addCriteria("Age", age);
		rule.addCriteria("Sex", new Dropdown<SexValues>());
		rule.addCriteria("Disabled", new Checkbox());
		rules.add(rule);

		
		

//		Rule rule = new Rule();
//		Range age = new Range();
//		age.setMin(20);
//		rule.addCriteria("Age", age);
//		rule.addCriteria("Sex", new Dropdown<SexValues>(SexValues.M));
//		rule.addCriteria("Disabled", new Checkbox());
//		rules.add(rule);
//
//		rule = new Rule();
//		age = new Range();
//		age.setMax(19);
//		rule.addCriteria("Age", age);
//		rule.addCriteria("Sex", new Dropdown<SexValues>(SexValues.M));
//		rule.addCriteria("Disabled", new Checkbox(true));
//		rules.add(rule);
		
//		rule = new Rule();
//		age = new Range();
//		age.setMax(19);
//		rule.addCriteria("Age", age);
//		rule.addCriteria("Sex", new Dropdown<SexValues>(SexValues.M));
//		rule.addCriteria("Disabled", new Checkbox(false));
//		rules.add(rule);
//		
//		rule = new Rule();
//		age = new Range();
//		age.setMin(15);
//		age.setMax(25);
//		rule.addCriteria("Age", age);
//		rule.addCriteria("Sex", new Dropdown<SexValues>(SexValues.M));
//		rule.addCriteria("Disabled", new Checkbox(true));
//		rules.add(rule);
		
		return rules;
	}
	
	public static HashMap<Integer, List<Integer>> detectCollisions(List<Rule> rules){
		HashMap<Integer, List<Integer>> collisions = new HashMap<Integer, List<Integer>>();
		
		for (int i = 0; i < rules.size() - 1; i++) {
			Rule rule = rules.get(i);
			int ruleId = rule.getIdNumber();
			
			for (int j = i + 1; j < rules.size(); j++) {
				Rule otherRule = rules.get(j);
				if (rule.conflicts(otherRule)){
					if (!collisions.containsKey(ruleId)){
						collisions.put(ruleId, new ArrayList<Integer>());
					}
					collisions.get(ruleId).add(otherRule.getIdNumber());
				}
			}
		}
		
		return collisions;
	}
}
