package com.example.main;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertNotEquals;

/**
 * Created by vturcanu on 3/10/2017.
 */
public class CollectionsCopyTest {

    @Test
    public void testArrayListShallowCopy() {
        ArrayList<String> list1 = new ArrayList<>();
        list1.add("First");
        list1.add("Second");
        list1.add("Third");

        ArrayList<String> list2 = new ArrayList<>(list1);
        list2.set(1, "Another Second");

        assertNotEquals(list1.get(1), list2.get(1));
    }
}
