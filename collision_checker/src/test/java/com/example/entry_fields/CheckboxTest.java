package com.example.entry_fields;

import static org.junit.Assert.*;
import org.junit.Test;

public class CheckboxTest {
	
	@Test
	public void testIsChecked(){
		Checkbox cb1 = new Checkbox();
		Checkbox cb2 = new Checkbox(false);
		Checkbox cb3 = new Checkbox(true);
		
		assertNull(cb1.isChecked());
		assertFalse(cb2.isChecked());
		assertTrue(cb3.isChecked());
		
		assertNotEquals(cb2.isChecked(), cb3.isChecked());
		assertNotEquals(cb1.isChecked(), cb2.isChecked());
		assertNotEquals(cb1.isChecked(), cb3.isChecked());
	}
	
	@Test
	public void testConflicts(){
		Checkbox cb1 = new Checkbox();
		Checkbox cb2 = new Checkbox(false);
		Checkbox cb3 = new Checkbox(true);
		
		assertTrue(cb1.conflicts(cb1));
		assertTrue(cb2.conflicts(cb2));
		assertTrue(cb3.conflicts(cb3));
		
		assertTrue(cb1.conflicts(cb2));
		assertTrue(cb2.conflicts(cb1));
		
		assertTrue(cb1.conflicts(cb3));
		assertTrue(cb3.conflicts(cb1));
		
		assertFalse(cb2.conflicts(cb3));
		assertFalse(cb3.conflicts(cb2));
	}
	
	@Test
	public void testSetChecked(){
		Checkbox c = new Checkbox();
		assertNull(c.isChecked());
		
		c.setChecked(true);
		assertTrue(c.isChecked());
		
		c.setChecked(false);
		assertFalse(c.isChecked());
		
		c.setChecked(true);
		assertTrue(c.isChecked());
		
		c.setChecked(null);
		assertNull(c.isChecked());
	}
}
