package com.example.entry_fields;

import static org.junit.Assert.*;
import org.junit.Test;

public class DropdownTest {
	@Test
	public void testGetValue(){
		Dropdown<SexValues> d = new Dropdown<SexValues>();
		assertEquals(d.getValue(), null);
		
		d = new Dropdown<SexValues>(SexValues.M);
		assertEquals(d.getValue(), SexValues.M);
	}
	
	@Test
	public void testConflicts(){
		Dropdown<SexValues> d1 = new Dropdown<SexValues>();
		Dropdown<SexValues> d2 = new Dropdown<SexValues>(SexValues.M);
		Dropdown<SexValues> d3 = new Dropdown<SexValues>(SexValues.F);
		Dropdown<SexValues> d4 = new Dropdown<SexValues>(SexValues.OTHER);
		
		assertTrue(d1.conflicts(d1));
		assertTrue(d2.conflicts(d2));
		assertTrue(d3.conflicts(d3));
		assertTrue(d4.conflicts(d4));
		
		assertTrue(d1.conflicts(d2));
		assertTrue(d2.conflicts(d1));
		
		assertTrue(d1.conflicts(d3));
		assertTrue(d3.conflicts(d1));
		
		assertTrue(d1.conflicts(d4));
		assertTrue(d4.conflicts(d1));

		assertFalse(d2.conflicts(d3));
		assertFalse(d3.conflicts(d2));
		
		assertFalse(d3.conflicts(d4));
		assertFalse(d4.conflicts(d3));
	}
}
