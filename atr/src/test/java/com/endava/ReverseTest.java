package com.endava;

import org.junit.Assert;
import org.junit.Test;

public class ReverseTest {

	@Test
	public void reverse() {
		Assert.assertEquals("cba", Reverse.reverse("abc"));
		Assert.assertFalse(Reverse.reverse("xyz").equals("zxs"));
	}
}
