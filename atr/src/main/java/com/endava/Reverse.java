package com.endava;

public class Reverse {

	public static String reverse(String str1) {
		if (str1.length() == 0)
			return str1;
		else
			return reverse(str1.substring(1)) + str1.charAt(0);
	}
}
