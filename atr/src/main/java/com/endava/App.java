package com.endava;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        AtomicInteger a = new AtomicInteger(1);
        change(a);
        System.out.println(a);
    }
    
    public static void change(AtomicInteger value){
    	value.set(2);
    }
}
