package com.example.jdbc_example;

import java.sql.*;
/**
 * Hello world!
 *
 */
public class App {
  public static void main(String[] args) throws SQLException, ClassNotFoundException {
    String url = "jdbc:postgresql://localhost:5432/zoo";
    
    Class.forName("org.postgresql.Driver");
    Connection conn = DriverManager.getConnection(url, "postgres", "admin");
    System.out.println(conn);
  }
}
