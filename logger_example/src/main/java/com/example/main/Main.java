package com.example.main;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vturcanu on 2/17/2017.
 */
public class Main {

    static Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        LOGGER.info("This is an info");
        LOGGER.error("This is an error");
    }
}
