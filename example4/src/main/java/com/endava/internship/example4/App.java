package com.endava.internship.example4;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        try {
        	String filePath = args[0];
        	TransactionsHandler handler = new TransactionsHandler();
        	handler.parseTransactions(filePath);
        	
        } catch (IndexOutOfBoundsException ex) {
        	System.out.println("It must be specified the path of the transactions file.");
        } catch (IOException ex) {
        	ex.printStackTrace();
        }
    }
    
    public int seq(int number){
    	if (number % 2 == 0) {
    		return number / 2;
    	} else {
    		return number * 3 + 1;
    	}
    }
}
