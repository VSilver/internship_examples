package com.endava.internship.example4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vturcanu
 *
 */
public class TransactionsHandler {
	private Map<String, HashMap<String, Integer>> transactions = new HashMap<String, HashMap<String, Integer>>();

	public Map<String, HashMap<String, Integer>> getTransactions() {
		return transactions;
	}

	public void parseTransactions(String filePath) throws IOException{
		try (BufferedReader reader = new BufferedReader(new FileReader(filePath))){
			String s;
			while((s = reader.readLine()) != null){
				parseLine(s);
			}
		} 
	}
	
	public void parseLine(String line){
		String[] words = line.split(",");
		
		if (!transactions.containsKey(words[0])){
			transactions.put(words[0], new HashMap<String, Integer>());
		} 
		transactions.get(words[0]).put(words[1], Integer.parseInt(words[2]));
		
		System.out.println(line.split(","));
	}
	
	
	public HashMap<String, Integer> computeAccountTotal(){
		HashMap<String, Integer> results = new HashMap<String, Integer>();
		for (String key:transactions.keySet()){
			int total = 0;
			for (String inner:transactions.get(key).keySet()){
				total += transactions.get(key).get(inner);
			}
			results.put(key, total);
		}
		return results;
	}
	
	public HashMap<String, HashMap<Integer, Integer>> computeProductQuantity(){
		HashMap<String, HashMap<Integer, Integer>> productQuantities = new HashMap<String, HashMap<Integer, Integer>>();
		for (String account:transactions.keySet()){
			for (String product:transactions.get(account).keySet()){
				if(!productQuantities.containsKey(product)){
					productQuantities.put(product, new HashMap<Integer, Integer>());
				}
				int quantity = transactions.get(account).get(product);
				if(!productQuantities.get(product).containsKey(transactions.get(account).get(product))){
					productQuantities.get(product).put(quantity, 1);
				} else {
					int number = productQuantities.get(product).get(quantity);
					productQuantities.get(product).put(quantity, ++number);
				}
			}
		}
		return productQuantities;
	}
	
}
