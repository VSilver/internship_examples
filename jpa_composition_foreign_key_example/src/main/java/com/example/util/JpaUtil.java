package com.example.util;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 * Created by Victor on 03.04.2017.
 */
public class JpaUtil {
    private static final EntityManager entityManager;

    static {
        try {
            entityManager = Persistence.createEntityManagerFactory("composition-example").createEntityManager();
        } catch (Throwable ex) {
            System.err.println("Initial EntityManager creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }
}
