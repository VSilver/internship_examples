package com.example.entity;

import javax.persistence.*;
import java.util.List;


@Entity
public class Professor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    @ElementCollection
    @CollectionTable(name = "PROJECT",
            joinColumns = {
                @JoinColumn(name = "PROF_ID", referencedColumnName = "ID")
            })
    @Column(name = "PROJ_TITLE")
    @OrderColumn(name = "PROJ_NUMBER")
    private List<String> projects;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
