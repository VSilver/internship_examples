package com.example;

import com.example.util.JpaUtil;

import javax.persistence.EntityManager;

/**
 * Created by Victor on 03.04.2017.
 */
public class Main {

    public static void main(String[] args) {
        EntityManager em = null;

        System.out.println("Setting up the environment");

        try {
            em = JpaUtil.getEntityManager();
            em.getTransaction().begin();

            em.getTransaction().commit();
        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }
    }
}
