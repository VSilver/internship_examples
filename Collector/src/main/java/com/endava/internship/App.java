package com.endava.internship;

import com.endava.internship.collectors.MostFrequent;

import java.util.Optional;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {

        Optional<Integer> opt = Stream.of(1, 2, 3, 4, 5, 2, 3, 4, 2, 3, 34, 1, 23, 2, 2).collect(new MostFrequent<>());
        System.out.println(opt.get());

        Optional opt2 = Stream.of("a", "b", "c", "a").collect(new MostFrequent<>());
        System.out.println(opt2.get());

        int number_of_running_processors = Runtime.getRuntime().availableProcessors();
        System.out.println(number_of_running_processors);
    }
}
