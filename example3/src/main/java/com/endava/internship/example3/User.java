package com.endava.internship.example3;

public class User {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override 
	public boolean equals(Object obj){
		if (!(obj instanceof User)) return false;
		User other = (User) obj;
		return this.name.equals(other.name);
	}
	
	@Override
	public int hashCode(){
		return name.hashCode();
	}
}
