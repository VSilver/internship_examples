package com.endava.internship.example3;

import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Map<User, Integer> points= new HashMap<User, Integer>();
        
        User user = new User();
        user.setName("Vova");
        
        points.put(user, 10);
        
        user = new User();
        user.setName("Vova");
        
        System.out.println(points.get(user));
    }
}
