package route;

import org.apache.camel.builder.RouteBuilder;

/**
 * Created by vturcanu on 5/24/2017.
 */
public class MyRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("direct:foo")
                .process( exchange -> {
                    System.out.println(exchange.getIn().getBody());
                })
                .to("direct:bar");
    }
}
