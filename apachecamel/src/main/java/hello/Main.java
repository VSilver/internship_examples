package hello;


import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class Main {

    public static void main(String[] args) throws Exception{
        CamelContext camelContext = new DefaultCamelContext();
        camelContext.addRoutes(new RouteBuilder() {
            public void configure() {
                from("file:src/data/outbox?noop=true")
                        .log("ceva")
                        .to("file:src/data/inbox");
            }
        });

        camelContext.start();
        Thread.sleep(1000);
        camelContext.stop();
    }
}
