package hello;

import config.JavaConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * Created by vturcanu on 5/25/2017.
 */
@SpringBootApplication
public class Application {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    @Qualifier("myInterface1")
    private MyInterface myInterface;

    public static void main(String[] args) {
        SpringApplication.run(new Class<?>[]{Application.class, JavaConfig.class}, args);
    }

    @Bean
    public CommandLineRunner demo(@Qualifier("myInterface1") final MyInterface myInterface) {
        return (args) -> {
            System.out.println(myInterface.hello(1L));
        };
    }
}
