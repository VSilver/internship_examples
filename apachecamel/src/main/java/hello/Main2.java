package hello;


import java.io.*;

public class Main2 {

    public static void main(String[] args) throws Exception{
        File inboxDirectory = new File("src/data/outbox");
        File outboxDirectory = new File("src/data/inbox");

        outboxDirectory.mkdir();

        File[] files = inboxDirectory.listFiles();

        try {
            for (File source : files) {
                if (source.isFile()) {
                    File dest = new File(
                            outboxDirectory.getPath()
                                    + File.separator
                                    + source.getName());
                    copyFile(source, dest);
                }
            }
        } catch (NullPointerException ex) {
            System.err.println("The directory does not contain any files");
        }
    }

    private static void copyFile(File source, File destination) throws IOException {
        OutputStream out = new FileOutputStream(destination);
        InputStream in = new FileInputStream(source);
        byte[] buffer = new byte[(int) source.length()];

        in.read(buffer);

        try {
            out.write(buffer);
        } finally {
            out.close();
            in.close();
        }
    }
}
