package route;

import config.TestConfig;
import hello.MyInterface;
import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.builder.ProxyBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringDelegatingTestContextLoader;
import org.apache.camel.test.spring.CamelSpringRunner;
import org.apache.camel.test.spring.CamelSpringTestContextLoader;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.junit.Assert.assertEquals;

/**
 * Created by vturcanu on 5/24/2017.
 */
@RunWith(CamelSpringRunner.class)
@SpringApplicationConfiguration(classes = TestConfig.class)
public class MyRouteTest {

    @EndpointInject(uri = "mock:tar")
    private MockEndpoint resultEndpoint;

    @Autowired
    private MyInterface myInterface;

    @Test
    public void testRoute() throws Exception {
        myInterface.hello(123L);
        resultEndpoint.expectedMessageCount(1);
        final Long body = resultEndpoint.getExchanges().get(0).getIn().getBody(Long.class);
        System.out.println(body.toString());

        resultEndpoint.assertIsSatisfied(2000);
    }
}
