package config;

import hello.MyInterface;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.ProxyBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.javaconfig.CamelConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import route.MyRoute;

/**
 * Created by vturcanu on 5/24/2017.
 */
@Configuration
public class TestConfig extends CamelConfiguration {

    @Bean
    public MyInterface myInterface(final CamelContext camelContext) throws Exception {
        return new ProxyBuilder(camelContext).endpoint("direct:foo").build(MyInterface.class);
    }

    @Bean
    public MyRoute myRoute() {
        return new MyRoute();
    }

    @Bean
    public RouteBuilder toMockEndpoint() {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("direct:bar")
                        .process( exchange -> {
                            System.out.println("-------------------------------");
                            System.out.println(exchange.getIn().getBody());
                            System.out.println("-------------------------------");
                        })
                        .to("mock:tar");
            }
        };
    }
}
