package com.example.route;

import com.example.config.Config;
import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;

import java.util.Collections;

@ContextConfiguration(classes = {Config.class})
public class MyRouteTest extends CamelTestSupport {

    private static final String DIRECT_TO = "direct:to";
    private static final String DIRECT_IN = "direct:in";
    private static final String DIRECT_OUT = "direct:out";
    private static final String MOCK_RESULT = "mock:result";

    @Produce(uri = "direct:to")
    private ProducerTemplate producerTemplate;

    @EndpointInject(uri = "mock:result")
    private MockEndpoint resultEndpoint;

    @Override
    public RouteBuilder[] createRouteBuilders() {
        RouteBuilder myRoute = new MyRoute();

        RouteBuilder in = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from(DIRECT_TO)
                        .to(DIRECT_IN);
            }
        };

        RouteBuilder result = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from(DIRECT_OUT)
                        .to(MOCK_RESULT);
            }
        };

        return new RouteBuilder[] {in, myRoute, result};
    }

    @Test
    @Rollback(false)
    public void testRoute() throws InterruptedException {

        resultEndpoint.expectedMessageCount(1);
        resultEndpoint.expectedBodiesReceived(Collections.singletonList(6));

        producerTemplate.sendBody("1");
        producerTemplate.sendBody("2");
        producerTemplate.sendBody("3");

        resultEndpoint.assertIsSatisfied(10000);
//        resultEndpoint.assertExchangeReceived(1);
    }
}
