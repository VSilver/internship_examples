package com.example.beans;

import org.apache.camel.processor.aggregate.jdbc.JdbcAggregationRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Component
public class JdbcAggregationRepositoryImpl extends JdbcAggregationRepository {

    public JdbcAggregationRepositoryImpl(PlatformTransactionManager transactionManager, DataSource dataSource) {
        super(transactionManager, "myRepository", dataSource);
    }
}
