package com.example.config;

import com.example.BasePackage;
import org.apache.camel.processor.aggregate.jdbc.JdbcAggregationRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackageClasses = {BasePackage.class})
public class Config {

    private static final String PERSISTENCE_UNIT_NAME = "MY_PERSISTENT_UNIT";

    private static final String PERSISTENCE_PACKAGES_TO_SCAN = "com.example.persistence";

    private static final String PERSISTENCE_XML_LOCATION = "classpath*:META-INF/persistence.xml";

//    @Value("${dataSource.driverClass}")
//    private String driver;
//
//    @Value("${dataSource.url}")
//    private String url;
//
//    @Value("${dataSource.username}")
//    private String username;
//
//    @Value("${dataSource.password}")
//    private String password;
//
//    @Value("${hibernate.dialect}")
//    private String dialect;
//
//    @Value("${hibernate.show_sql}")
//    private String shoq_sql;
//
//    @Value("${hibernate.format_sql}")
//    private String format_sql;
//
//    @Value("${hibernate.hbm2ddl.auto}")
//    private String hbm2ddlAuto;
//
//    @Bean
//    public NativeJdbcExtractor nativeJdbcExtractor() {
//        return new SimpleNativeJdbcExtractor();
//    }
//
//    @Bean
//    public DefaultLobHandler lobHandler(NativeJdbcExtractor nativeJdbcExtractor) {
//        return new DefaultLobHandler();
//    }
//
//    @Bean(name = "myDataSource")
//    public DataSource dataSource() {
//
//        HikariConfig config = new HikariConfig();
//
//        config.setDriverClassName(driver);
//        config.setJdbcUrl(url);
//        config.setUsername(username);
//        config.setPassword(password);
//
//        return new HikariDataSource(config);
//    }
//
//    @Bean
//    public EntityManagerFactory entityManagerFactory(@Qualifier("myDataSource") DataSource dataSource) {
//
//        final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
//
//        entityManagerFactoryBean.setDataSource(dataSource);
//        entityManagerFactoryBean.setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
//        entityManagerFactoryBean.setPackagesToScan(PERSISTENCE_PACKAGES_TO_SCAN);
//        entityManagerFactoryBean.setPersistenceXmlLocation(PERSISTENCE_XML_LOCATION);
//        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
//        entityManagerFactoryBean.afterPropertiesSet();
//
//        return entityManagerFactoryBean.getObject();
//    }
//
//    @Bean
//    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) throws Throwable {
//        return new JpaTransactionManager(entityManagerFactory);
//    }

//    @Bean
//    public JdbcAggregationRepository myRepository(@Qualifier("dataSource") DataSource dataSource, /*LobHandler lobHandler,*/ PlatformTransactionManager transactionManager) {
//
//        JdbcAggregationRepository repository = new JdbcAggregationRepository();
//
//        repository.setTransactionManager(transactionManager);
//        repository.setRepositoryName("myRepository");
//        repository.setDataSource(dataSource);
////        repository.setLobHandler(lobHandler);
////        repository.setUseRecovery(true);
//
//        return repository;
//    }
}
