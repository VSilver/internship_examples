package com.example.route;

import com.example.aggregator.MyAggregationStrategy;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.processor.aggregate.jdbc.JdbcAggregationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyRoute extends RouteBuilder {

    @Autowired
    private JdbcAggregationRepository myRepository;

    public void configure() throws Exception {
        from("direct:in")
                .log("Consuming ${body}")
                .aggregate(constant(true), new MyAggregationStrategy())
                    .aggregationRepository(myRepository)
                    .completionSize(4)
                    .log("Sending out ${}")
                .to("direct:out");

    }
}
