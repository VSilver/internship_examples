package com.example;

import java.net.Inet4Address;
import java.util.Comparator;

/**
 * Created by Vi on 10/8/2016.
 */
public class Duck implements Comparable<Duck>{
    private String name;
    private Integer weight;

    public Duck(String name, Integer weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder().append("(").append(this.name).append(", ").append(this.weight).append(")");
        return sb.toString();
    }

    public int compareTo(Duck other) {
        return name.compareTo(other.getName());
    }
}
