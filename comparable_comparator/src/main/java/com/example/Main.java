package com.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Vi on 10/8/2016.
 */
public class Main {
    public static void main(String[] args){
        Comparator<Duck> byWeight = new Comparator<Duck>() {
            public int compare(Duck o1, Duck o2) {
                return o1.getWeight() - o2.getWeight();
            }
        };

        Comparator<Duck> byWeightThenByName = Comparator.comparingInt(d1 -> d1.getWeight());
        byWeightThenByName = byWeightThenByName.thenComparing(Duck::getName);

        Comparator<Duck> workingWithNullComparator = new Comparator<Duck>() {
            @Override
            public int compare(Duck d1, Duck d2) {
                return (d1.getWeight() == null || d2.getWeight() == null) ? 0 : (d1.getWeight() - d2.getWeight());
            }
        };

        Comparator<Duck> elegantComparator = DuckHelper::compareByWeight;

        List<Duck> ducks = new ArrayList<>();
        ducks.add(new Duck("Quack", 7));
        ducks.add(new Duck("Puddles", 11));
        ducks.add(new Duck("Doberman", 11));
//        ducks.add(new Duck("Doberman", null));


        Collections.sort(ducks, byWeight);                          // crashes for null value weight field
        System.out.println(ducks);                                  // [Quack, Puddles]
        Collections.sort(ducks, byWeightThenByName);                // crashes for null value weight field
//        Collections.sort(ducks, workingWithNullComparator);       // doesn't crash for null value weight field
        System.out.println(ducks);
        Collections.sort(ducks);
        System.out.println(ducks);                                  // [Puddles, Quack]
    }
}
