package com.example;

/**
 * Created by Vi on 10/8/2016.
 */
public class DuckHelper {
    public static int compareByWeight(Duck d1, Duck d2) {
        return (d1.getWeight() == null || d2.getWeight() == null) ? 0 : (d1.getWeight() - d2.getWeight());
    }

    public static int compareByName(Duck d1, Duck d2) {
        return d1.getName().compareTo(d2.getName());
    }
}
