package com.example.static_block;

/**
 * Hello world!
 *
 */
public class App 
{
	public App(){
		System.out.println("Constructor");
	}
	
	static {
		System.out.println("Inside static block");
	}
	
	{
		System.out.println("Inside dynamic block");
	}
	
    public static void main( String[] args )
    {
    	App app = new App();
    	App app0 = new App();
    	App app1 = new App2();
    	App2 app2 = new App2();
        System.out.println("Inside main");
    }
}
