package com.example.static_block;

public class App2 extends App{
	static {
		System.out.println("Inside static block of App2");
	}
	
	public App2(){
		System.out.println("Inside constructor of App2");
	}
	
	{
		System.out.println("Inside dynamic block of App2");
	}
}
