package com.endava;

import com.endava.route.MyRoute;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.spring.javaconfig.CamelConfiguration;
import org.apache.camel.spring.javaconfig.Main;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

import javax.jms.ConnectionFactory;


@Configuration
@ComponentScan(basePackages = {"com.endava"}, excludeFilters =
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = MyRoute.class))
public class AppConfiguration extends CamelConfiguration {

//    @Autowired
//    MyRoute myRoute;

    public static void main(String[] args) throws Exception {
        Main main = new Main();
        main.setConfigClass(AppConfiguration.class);
        main.run();
    }

    @Override
    protected void setupCamelContext(CamelContext camelContext) throws Exception {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost");
        camelContext.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
//        camelContext.addRoutes(myRoute);
    }

}
