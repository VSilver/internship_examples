package com.endava.beans;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.springframework.stereotype.Service;


@Service
public class MyBean {

    public void print(@Header(Exchange.FILE_NAME) String filename, @Body String body) {
        System.out.println("Received file " + filename);
        System.out.println("Content: \n" + body);
    }

}
