package com.endava.beans;


import org.apache.camel.RecipientList;
import org.apache.camel.language.XPath;
import org.springframework.stereotype.Service;


@Service
public class RecipientListBean {

    @RecipientList
    public String[] route(@XPath("//city/text()") String customer) {
        if (isGoldCustomer(customer)) {
            return new String[] {"jms:accounting", "jms:production"};
        } else {
            return new String[] {"jms:accounting"};
        }
    }

    private boolean isGoldCustomer(String customer) {
        return customer.equals("London");
    }
}
