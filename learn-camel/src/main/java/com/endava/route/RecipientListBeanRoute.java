package com.endava.route;


import com.endava.beans.RecipientListBean;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class RecipientListBeanRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        ExecutorService executor = Executors.newFixedThreadPool(5);

        from("file:src/data?noop=true")
                .log("--downloaded orders")
                .to("jms:incomingOrders");

        from("jms:incomingOrders")
                .wireTap("jms:orderAudit")
                .choice()
                    .when(header("CamelFileName").endsWith(".xml"))
                        .to("jms:xmlOrders")
                    .when(header("CamelFileName").regex("^.*(csv|csl)$"))
                        .to("jms:csvOrders")
                    .otherwise()
                        .to("jms:badOrders").stop()
                    .end()
                .to("jms:continueProcessing");

        from("jms:xmlOrders").bean(RecipientListBean.class)
                .recipientList(header("recipients"));

        from("jms:accounting")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        System.out.println("Accounting received order: "
                                + exchange.getIn().getHeader("CamelFileName"));
                    }
                });

        from("jms:production")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        System.out.println("Production received order: "
                                + exchange.getIn().getHeader("CamelFileName"));
                    }
                });
    }
}
