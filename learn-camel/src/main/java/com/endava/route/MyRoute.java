package com.endava.route;

import com.endava.beans.MyBean;
import com.endava.processor.MyProcessor;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Component
public class MyRoute extends RouteBuilder {

    @Autowired
    private MyProcessor myProcessor;

    @Autowired
    private MyBean myBean;

    @Override
    public void configure() throws Exception {
//        from("file:src/data?noop=true")
//                .log("--starting to read")
//                .beans(myBean, "print")
//                .log("--finished reading")
//                .to("file:src/data/out");

        ExecutorService executor = Executors.newFixedThreadPool(16);

        from("file:src/data?noop=true")
                .log("--downloaded orders")
                .to("jms:incomingOrders");

        from("jms:incomingOrders")
                .choice()
                    .when(header("CamelFileName").endsWith(".xml"))
                        .to("jms:xmlOrders")
                    .when(header("CamelFileName").regex("^.*(csv|csl)$"))
                        .to("jms:csvOrders")
                    .otherwise()
                        .to("jms:badOrders").stop()
                .end()
                .to("jms:continueProcessing");

//        from("jms:xmlOrders")
//                .filter(xpath("/person[not(./firstName/text() = 'James')]"))
//                .process(new Processor() {
//                    @Override
//                    public void process(Exchange exchange) throws Exception {
//                        System.out.println("Received XML order: "
//                                + exchange.getIn().getHeader("CamelFileName"));
//                    }
//                })
//                .multicast().stopOnException()
//                .parallelProcessing().executorService(executor)
//                .to("jms:accounting", "jms:production");

        from("jms:xmlOrders")
                .setHeader("customer", xpath("//city/text()"))
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {

                        String recipients = "jms:accounting";
                        String customer = exchange.getIn().getHeader("customer", String.class);

                        if (customer.equals("London")) {
                            recipients += ", jms:production";
                        }

                        exchange.getIn().setHeader("recipients", recipients);
                    }
                })
                .multicast().stopOnException()
                .parallelProcessing().executorService(executor)
                .recipientList(header("recipients"));

        from("jms:csvOrders")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        System.out.println("Received CSV order: "
                                + exchange.getIn().getHeader("CamelFileName"));
                    }
                });

        from("jms:badOrders")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        System.out.println("Received a bad order: "
                                + exchange.getIn().getHeader("CamelFileName"));
                    }
                });

        from("jms:accounting")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        System.out.println("Accounting received order: "
                                + exchange.getIn().getHeader("CamelFileName"));
                    }
                });

        from("jms:production")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        System.out.println("Production received order: "
                                + exchange.getIn().getHeader("CamelFileName"));
                    }
                });
    }
}
