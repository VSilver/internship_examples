
public class HelloWorld {
	
	public static void main(String[] args){
		
		String str1 = "abcd";
		String str2 = reverse(str1);
//		String str2 = new StringBuilder().append("").append(str1.charAt(2)).toString();
		System.out.println(str2);
	}

	private static String reverse(String str1) {
		if (str1.length() == 0)
			return str1;
		else
			return reverse(str1.substring(1)) + str1.charAt(0);
	}
}
