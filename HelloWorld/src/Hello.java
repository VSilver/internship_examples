/**
 * Created by vturcanu on 2/23/2017.
 */
public class Hello {
    public static void main(String[] args) {
        doSomething("", 23);
    }

    public static <T> void doSomething(T first, T second) {
        System.out.println("Do something");
    }

    static void iaka(Object a) {
        System.out.println(a.getClass());
    }
}
