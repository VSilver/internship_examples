package com.example.inheritance;

/**
 * Created by vturcanu on 3/31/2017.
 */
public class Second extends First {
    public void first() {
        System.out.println("2.1");
//        super.first();
    }

    public void second() {
        System.out.println("2.2");
        super.second();
    }

    public static void main(String[] args) {
        First first = new Second();
        first.second();
    }
}
