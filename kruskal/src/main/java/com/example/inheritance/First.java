package com.example.inheritance;

/**
 * Created by vturcanu on 3/31/2017.
 */
public class First {
    public void first() {
        System.out.println("1.1");
    }

    public void second() {
        System.out.println("1.2");
        first();
    }
}
