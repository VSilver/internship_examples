package com.example;

import com.example.kruskal.Edge;
import com.example.kruskal.Vertex;

import java.util.*;

/**
 * Created by vturcanu on 3/27/2017.
 */
public class Main {
    public static void main(String[] args) {
        List<Edge> edges = new ArrayList<>();
        edges.add(new Edge("A", "B", 1));
        edges.add(new Edge("B", "C", 5));
        edges.add(new Edge("C", "D", 3));
        edges.sort((o1, o2) -> o2.getWeight() - o1.getWeight());
        edges.forEach(System.out::println);

        Set<Vertex> vertices = new HashSet<>();
        vertices.add(new Vertex("A"));
        System.out.println(vertices.contains(new Vertex("A")));
    }
}
