package com.example.kruskal;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by vturcanu on 3/27/2017.
 */
public class Edge implements Comparable<Edge> {
    private Vertex from;
    private Vertex to;
    private int weight;

    @Override
    public int compareTo(Edge edge) {
        if (edge == this) {
            return 0;
        }

        return this.weight - edge.weight;
    }

    public Edge(String from, String to, int weight) {
        this.from = new Vertex(from);
        this.to = new Vertex(to);
        this.weight = weight;
    }

    public Edge(Vertex from, Vertex to, int weight) {
        this.from = from;
        this.to = to;
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Vertex getFrom() {
        return from;
    }

    public void setFrom(Vertex from) {
        this.from = from;
    }

    public Vertex getTo() {
        return to;
    }

    public void setTo(Vertex to) {
        this.to = to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Edge)) return false;

        Edge edge = (Edge) o;
        return new EqualsBuilder()
                .appendSuper(super.equals(edge))
                .append(from, edge.getFrom())
                .append(to, edge.getTo())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(from)
                .append(to)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "Edge{" +
                "from=" + from.getName() +
                ", to=" + to.getName() +
                ", weight=" + weight +
                '}';
    }
}
