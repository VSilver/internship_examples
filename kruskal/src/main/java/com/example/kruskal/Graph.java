package com.example.kruskal;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by vturcanu on 3/27/2017.
 */
public class Graph {
    private Set<Vertex> vertices;
    private Set<Edge> edges;

    public Graph() {
    }

    public Vertex addVertex(Vertex vertex) {
        if (!vertices.contains(vertex)) {
            vertices.add(vertex);
            return vertex;
        } else {
            Iterator<Vertex> iterator = vertices.iterator();
            Vertex local = null;
            while (iterator.hasNext()) {
                local = iterator.next();
                if (local.equals(vertex)) break;
            }
            return local;
        }
    }

    public Set<Vertex> getVertices() {
        return new HashSet<Vertex>(vertices);
    }

    public Edge addEdge(Edge edge) {
        if (!edges.contains(edge)) {
            edges.add(edge);
            return edge;
        } else {
            Iterator<Edge> iterator = edges.iterator();
            Edge local = null;
            while (iterator.hasNext()) {
                local = iterator.next();
                if (local.equals(edge)) break;
            }
            return local;
        }
    }

    public Set<Edge> getEdges() {
        return new TreeSet<Edge>(edges);
    }

    public boolean loadFromFile(String filePath) {
        try (BufferedReader bufferReader = new BufferedReader(new FileReader(filePath))) {
            StringBuilder stringBuilder = new StringBuilder();
            String line = bufferReader.readLine();

            while (line != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
                line = bufferReader.readLine();
            }

            String fileString = stringBuilder.toString();
            String[] tokens = fileString.split(System.lineSeparator());

            for (int fromVertex = 0; fromVertex < tokens.length; fromVertex++) {
                String[] subTokens = tokens[fromVertex].split(",");
                for (int toVertex = 0; toVertex < subTokens.length; toVertex++) {
                    if (!subTokens[toVertex].equals('-')) {
                        Vertex from = new Vertex((new Integer(fromVertex)).toString());
                        Vertex to = new Vertex((new Integer(toVertex)).toString());
                        from = this.addVertex(from);
                        to = this.addVertex(to);
                        Edge edge = new Edge(from, to, Integer.parseInt(subTokens[toVertex]));
                        this.addEdge(edge);
                    }
                }
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public Set<Edge> getAllEdgesOfVertex(Vertex vertex) {
        return edges.stream()
                .filter(e -> e.getFrom().equals(vertex) || e.getTo().equals(vertex))
                .collect(Collectors.toSet());
    }
}
