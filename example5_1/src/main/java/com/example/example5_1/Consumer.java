package com.example.example5_1;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable{
	BlockingQueue<Integer> queue;
	
	public Consumer(BlockingQueue<Integer> queue){
		this.queue = queue;
	}
	
	public void run(){
		try{
			while(true){
				int value = queue.take();
				System.out.println(value);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
