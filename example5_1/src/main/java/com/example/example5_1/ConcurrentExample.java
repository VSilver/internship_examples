package com.example.example5_1;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Hello world!
 *
 */
public class ConcurrentExample 
{
    public static void main( String[] args )
    {
    	BlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>();
    	ExecutorService executors = Executors.newFixedThreadPool(3);
    	
    	executors.submit(new Producer(queue));
    	executors.submit(new Consumer(queue));
    	executors.submit(new Consumer(queue));
    }
}
