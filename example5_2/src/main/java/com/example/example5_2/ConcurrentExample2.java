package com.example.example5_2;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Hello world!
 *
 */
public class ConcurrentExample2 
{
    public static void main( String[] args )
    {
    	Queue<Integer> queue = new LinkedList<Integer>();
        ExecutorService executors = Executors.newFixedThreadPool(3);
        
        executors.submit(new Producer(queue));
        executors.submit(new Consumer(queue));
        executors.submit(new Consumer(queue));
    }
}
