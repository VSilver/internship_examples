package com.example.example5_2;

import java.util.Queue;
import java.util.Random;

public class Producer implements Runnable {
	private Queue<Integer> queue;

	public Producer(Queue<Integer> queue) {
		this.queue = queue;
	}

	public void run() {
		while (true) {
			synchronized (queue) {
				while (queue.size() == 8) {
					try {
						System.out.println("Queue is full, " + "Producer thread waiting for "
								+ "consumer to take something from queue");
						queue.wait();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				Random random = new Random();
				int i = random.nextInt();
				System.out.println("Producing value : " + i);
				queue.add(i);
				queue.notifyAll();
			}
		}
	}
}
