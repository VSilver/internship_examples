package com.example.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class Feature extends Issue{
  private Integer estimate;
  
  @Enumerated(EnumType.STRING)
  private MoscowEnum moscow;
  
  public Feature() {super();}
  
  public Integer getEstimate() {
    return estimate;
  }
  public void setEstimate(Integer estimate) {
    this.estimate = estimate;
  }
  public MoscowEnum getMoscow() {
    return moscow;
  }
  public void setMoscow(MoscowEnum moscow) {
    this.moscow = moscow;
  }
}
