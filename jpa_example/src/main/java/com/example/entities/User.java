package com.example.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  
  private String email;
  
  @ElementCollection
  private List<String> nicknames = new ArrayList<String>();
  
  @OneToMany(mappedBy = "reported")
  private List<Issue> reportedIssues;
  
  @OneToMany(mappedBy = "assignee")
  private List<Issue> assignedIssues;
  
  @ManyToMany(mappedBy = "watchers")
  private List<Issue> watchedIssues;
  
  public User() {
    super();
  }
  
  public User(String email, List<String> nicknames, List<Issue> reportedIssues,
      List<Issue> assignedIssues) {
    super();
    this.email = email;
    this.nicknames = nicknames;
    this.reportedIssues = reportedIssues;
    this.assignedIssues = assignedIssues;
  }

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public List<String> getNicknames() {
    return nicknames;
  }
  public void setNicknames(List<String> nicknames) {
    this.nicknames = nicknames;
  }
  public List<Issue> getReportedIssues() {
    return reportedIssues;
  }
  public void setReportedIssues(List<Issue> reportedIssues) {
    this.reportedIssues = reportedIssues;
  }
  public List<Issue> getAssignedIssues() {
    return assignedIssues;
  }
  public void setAssignedIssues(List<Issue> assignedIssues) {
    this.assignedIssues = assignedIssues;
  }
}
