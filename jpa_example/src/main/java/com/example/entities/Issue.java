package com.example.entities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

@Entity
@MappedSuperclass
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Issue {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  protected Date dateCreated;

  protected String description;

  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  @JoinColumn(name = "reported_userid")
  protected User reported;

  @ManyToOne(optional = true)
  protected User assignee;

  @ManyToMany
  protected List<User> watchers = new ArrayList<User>();

  @OneToMany(fetch = FetchType.LAZY)
  protected List<Comment> comments = new ArrayList<Comment>();

  @OneToMany(fetch = FetchType.LAZY)
  protected List<Attachment> attachments;

  @Enumerated(EnumType.STRING)
  protected StatusEnum status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(Date dateCreated) {
    this.dateCreated = dateCreated;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public User getReported() {
    return reported;
  }

  public void setReported(User reported) {
    this.reported = reported;
  }

  public User getAssignee() {
    return assignee;
  }

  public void setAssignee(User assignee) {
    this.assignee = assignee;
  }

  public List<User> getWatchers() {
    return watchers;
  }

  public void setWatcher(User watcher) {
    this.watchers.add(watcher);
  }

  public void removeWatcher(User watcher) {
    this.watchers.remove(watcher);
  }

  public List<Comment> getComments() {
    return comments;
  }

  public void setComment(Comment comment) {
    this.comments.add(comment);
  }
  
  public void removeComment(Comment comment) {
    this.comments.remove(comment);
  }

  public List<Attachment> getAttachments() {
    return attachments;
  }

  public void setAttachments(List<Attachment> attachments) {
    this.attachments = attachments;
  }

  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }
}
