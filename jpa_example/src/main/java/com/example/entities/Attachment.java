package com.example.entities;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Attachment {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private User user;
  private Date dateUploaded;
  private String fileName;
  private Byte[] data;
  
  public Attachment() {}
  
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public User getUser() {
    return user;
  }
  public void setUser(User user) {
    this.user = user;
  }
  public Date getDateUploaded() {
    return dateUploaded;
  }
  public void setDateUploaded(Date dateUploaded) {
    this.dateUploaded = dateUploaded;
  }
  public String getFileName() {
    return fileName;
  }
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }
  public Byte[] getData() {
    return data;
  }
  public void setData(Byte[] data) {
    this.data = data;
  }
  
}
