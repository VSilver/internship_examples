package com.example.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;

@Entity
public class Defect {
  private String environment;
  
  @Enumerated(EnumType.STRING)
  private ResolutionEnum resolution;
  
  @OneToMany
  private List<Defect> duplicates;
  
  public Defect() {super();}
  
  public String getEnvironment() {
    return environment;
  }
  public void setEnvironment(String environment) {
    this.environment = environment;
  }
  public ResolutionEnum getResolution() {
    return resolution;
  }
  public void setResolution(ResolutionEnum resolution) {
    this.resolution = resolution;
  }
  public List<Defect> getDuplicates() {
    return duplicates;
  }
  public void setDuplicates(List<Defect> duplicates) {
    this.duplicates = duplicates;
  }
}
