package com.example.jpa_example;

import com.example.dao.CommentDAO;
import com.example.dao.FeatureDAO;
import com.example.dao.UserDAO;
import com.example.entities.Comment;
import com.example.entities.Feature;
import com.example.entities.User;
import com.example.util.CommentDAOFactory;
import com.example.util.FeatureDAOFactory;
import com.example.util.UserDAOFactory;


public class App {
  public static void main(String[] args) {
    CommentDAO commentDAO = CommentDAOFactory.getInstance().getCommentDAO();
    UserDAO userDAO = UserDAOFactory.getInstance().getUserDAO();
    FeatureDAO featureDAO = FeatureDAOFactory.getInstance().getUserDAO();
    
    Comment comment = new Comment();
    User user = new User();
    Feature feature = new Feature();
    
    user.setEmail("email_1");
    comment.setText("comment text");
    feature.setDescription("feature description");
    feature.setEstimate(2);
    
    comment.setUser(user);
    feature.setComment(comment);
    
    userDAO.create(user);
    commentDAO.create(comment);
    featureDAO.create(feature);
    
    
//    Employee employee = new Employee("Alex", "Bob", 123456.);
//    employeeDAOFactory.getEmployeeDAO().create(employee);
//
//    Employee employee2 = employeeDAOFactory.getEmployeeDAO().getOne(employee.getId());
//    System.out.println(employee2);
//
//    HibernateUtil.getSessionFactory().close();
  }
}
