package com.example.util;

import com.example.dao.AttachmentDAO;
import com.example.dao.AttachmentDAOImpl;

public class AttachmentDAOFactory {
  private static AttachmentDAOFactory instance;
  private static AttachmentDAO attachmentDAO;
  
  private AttachmentDAOFactory() {}
  
  public AttachmentDAOFactory getInstance() {
    if (instance == null) {
      instance = new AttachmentDAOFactory();
    }
    return instance;
  }
  
  public AttachmentDAO getAttachmentDAO() {
    if (attachmentDAO == null) {
      attachmentDAO = new AttachmentDAOImpl();
    }
    return attachmentDAO;
  }
}
