package com.example.util;

import com.example.dao.CommentDAO;
import com.example.dao.CommentDAOImpl;

public class CommentDAOFactory {
  private static CommentDAOFactory instance;
  private static CommentDAO commentDAO;
  
  private CommentDAOFactory() {}
  
  public static CommentDAOFactory getInstance() {
    if (instance == null) {
      instance = new CommentDAOFactory();
    }
    return instance;
  }
  
  public CommentDAO getCommentDAO() {
    if (commentDAO == null) {
      commentDAO = new CommentDAOImpl();
    }
    return commentDAO;
  }
}
