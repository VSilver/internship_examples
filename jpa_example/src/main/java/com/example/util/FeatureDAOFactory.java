package com.example.util;

import com.example.dao.FeatureDAO;
import com.example.dao.FeatureDAOImpl;

public class FeatureDAOFactory {
  private static FeatureDAOFactory instance;
  private static FeatureDAO featureDAO;
  
  private FeatureDAOFactory() {}
  
  public static synchronized FeatureDAOFactory getInstance() {
    if (instance == null) {
      instance = new FeatureDAOFactory();
    }
    return instance;
  }
  
  public synchronized FeatureDAO getUserDAO() {
    if (featureDAO == null) {
      featureDAO = new FeatureDAOImpl();
    }
    return featureDAO;
  }
}
