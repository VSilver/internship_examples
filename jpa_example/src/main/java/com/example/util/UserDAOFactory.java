package com.example.util;

import com.example.dao.UserDAO;
import com.example.dao.UserDAOImpl;

public class UserDAOFactory {
  private static UserDAOFactory instance;
  private static UserDAO userDAO;
  
  private UserDAOFactory() {}
  
  public static synchronized UserDAOFactory getInstance() {
    if (instance == null) {
      instance = new UserDAOFactory();
    }
    return instance;
  }
  
  public synchronized UserDAO getUserDAO() {
    if (userDAO == null) {
      userDAO = new UserDAOImpl();
    }
    return userDAO;
  }
}
