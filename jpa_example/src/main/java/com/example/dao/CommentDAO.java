package com.example.dao;

import com.example.entities.Comment;

public interface CommentDAO extends BaseDAO<Comment, Long> {

}
