package com.example.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.example.util.JPAUtil;

public abstract class BaseDAOImpl<T, K extends Serializable> implements BaseDAO<T, K> {
  
  private final Class<T> type;
  
  @SuppressWarnings("unchecked")
  BaseDAOImpl() {
    Type t = getClass().getGenericSuperclass();
    ParameterizedType pt = (ParameterizedType) t;
    type = (Class<T>) pt.getActualTypeArguments()[0];
  }
  
  public EntityManager getEntityManager() {
    return JPAUtil.getEntityManager();
  }
  
  public void create(T entity) {
    EntityManager em = null;

    try {
      em = getEntityManager();
      em.getTransaction().begin();
      em.persist(entity);
      em.getTransaction().commit();
    } finally {
      if (em != null && em.isOpen()) {
        em.close();
      }
    }
  }

  public T getOne(Long id) {
    EntityManager em = null;
    T entity = null;

    try {
      em = getEntityManager();
      em.getTransaction().begin();
      entity = em.find(type, id);
      em.getTransaction().commit();
    } finally {
      if (em != null && em.isOpen()) {
        em.close();
      }
    }
    return entity;
  }

  public List<T> getAll() {
    EntityManager em = null;
    List<T> entities = null;

    try {
      em = getEntityManager();
      em.getTransaction().begin();
      
      CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(type);
      Root<T> root = cq.from(type);
      cq.select(root);
      entities = em.createQuery(cq).getResultList();
      
      em.getTransaction().commit();
    } finally {
      if (em != null && em.isOpen()) {
        em.close();
      }
    }
    return entities;
  }

  public void update(T entity) {
    EntityManager em = null;

    try {
      em = getEntityManager();
      em.getTransaction().begin();
      em.persist(entity);
      em.getTransaction().commit();
    } finally {
      if (em != null && em.isOpen()) {
        em.close();
      }
    }
  }

  public void delete(Long id) {
    delete(getOne(id));
  }

  public void delete(T entity) {
    EntityManager em = null;

    try {
      em = getEntityManager();
      em.getTransaction().begin();
      em.remove(entity);
      em.getTransaction().commit();
    } finally {
      if (em != null && em.isOpen()) {
        em.close();
      }
    }
  }
}
