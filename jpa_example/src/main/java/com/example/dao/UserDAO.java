package com.example.dao;

import com.example.entities.User;

public interface UserDAO extends BaseDAO<User, Long> {

}
