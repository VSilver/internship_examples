package com.example.dao;

import com.example.entities.Issue;

public interface IssueDAO extends BaseDAO<Issue, Long> {

}
