package com.example.entities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Issue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    protected Date dateCreated;

    protected String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "reported_userid")
    protected User reporter;

    @ManyToOne(optional = true)
    @JoinColumn(name = "asignee_userid")
    protected User assignee;

    @ManyToMany
    @JoinTable(name = "watcher_issue")
    protected List<User> watchers = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Comment> comments = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "issueId")
    protected List<Attachment> attachments = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    protected StatusEnum status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getReporter() {
        return reporter;
    }

    public void setReporter(User reporter) {
        this.reporter = reporter;
    }

    public void removeReporter() {
        this.reporter = null;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public void removeAssignee() {
        this.assignee = null;
    }

    public List<User> getWatchers() {
        return watchers;
    }

    public void setWatcher(User watcher) {
        this.watchers.add(watcher);
    }

    public void removeWatcher(User watcher) {
        this.watchers.remove(watcher);
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComment(Comment comment) {
        this.comments.add(comment);
    }

    public void removeComment(Comment comment) {
        this.comments.remove(comment);
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Issue{" +
                "id=" + id +
                ", dateCreated=" + dateCreated +
                ", description='" + description + '\'' +
                ", reporter=" + reporter +
                ", assignee=" + assignee +
                ", watchers=" + watchers +
                ", comments=" + comments +
                ", attachments=" + attachments +
                ", status=" + status +
                '}';
    }
}
