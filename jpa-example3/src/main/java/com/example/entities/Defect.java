package com.example.entities;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;

@Entity
public class Defect extends Issue{
    private String environment;

    @Enumerated(EnumType.STRING)
    private ResolutionEnum resolution;

    @OneToMany
    @Cascade(CascadeType.PERSIST)
    private List<Defect> duplicates = new ArrayList<>();

    public Defect() {super();}

    public String getEnvironment() {
        return environment;
    }
    public void setEnvironment(String environment) {
        this.environment = environment;
    }
    public ResolutionEnum getResolution() {
        return resolution;
    }
    public void setResolution(ResolutionEnum resolution) {
        this.resolution = resolution;
    }
    public List<Defect> getDuplicates() {
        return duplicates;
    }
    public void setDuplicates(List<Defect> duplicates) {
        this.duplicates = duplicates;
    }

    @Override
    public String toString() {

        return super.toString() + "Defect{" +
                "environment='" + environment + '\'' +
                ", resolution=" + resolution +
                ", duplicates=" + duplicates +
                '}';
    }
}
