package com.example.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "Custom_User")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String email;

    @ElementCollection
    private List<String> nicknames = new ArrayList<>();

    @OneToMany(mappedBy = "reporter", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Issue> reportedIssues = new ArrayList<>();

    @OneToMany(mappedBy = "assignee", cascade = CascadeType.PERSIST)
    private List<Issue> assignedIssues = new ArrayList<>();

    @ManyToMany(mappedBy = "watchers", cascade = CascadeType.PERSIST)
    private List<Issue> watchedIssues = new ArrayList<>();

    public User() {
        super();
    }

    public User(String email, List<String> nicknames, List<Issue> reportedIssues,
                List<Issue> assignedIssues) {
        super();
        this.email = email;
        this.nicknames = nicknames;
        this.reportedIssues = reportedIssues;
        this.assignedIssues = assignedIssues;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getNicknames() {
        return nicknames;
    }

    public void setNicknames(List<String> nicknames) {
        this.nicknames = nicknames;
    }

    public void addNickname(String nickname) {
        this.nicknames.add(nickname);
    }

    public List<Issue> getReportedIssues() {
        return reportedIssues;
    }

    public void setReportedIssues(List<Issue> reportedIssues) {
        this.reportedIssues = reportedIssues;
    }

    public void addReportedIssue(Issue issue) {
        this.assignedIssues.add(issue);
        issue.setReporter(this);
    }

    public void removeReportedIssue(Issue issue) {
        issue.removeReporter();
        this.reportedIssues.remove(issue);
    }

    public List<Issue> getAssignedIssues() {
        return assignedIssues;
    }

    public void setAssignedIssues(List<Issue> assignedIssues) {
        this.assignedIssues = assignedIssues;
    }

    public void addAssignedIssue(Issue assignedIssue) {
        this.assignedIssues.add(assignedIssue);
        assignedIssue.setAssignee(this);
    }

    public void removeAssignedIssue(Issue toBeRemovedIssue) {
        this.assignedIssues.remove(toBeRemovedIssue);
        toBeRemovedIssue.removeAssignee();
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", nicknames=" + nicknames +
                ", reportedIssues=" + reportedIssues +
                ", assignedIssues=" + assignedIssues +
                ", watchedIssues=" + watchedIssues +
                '}';
    }

    public String minimalToString() {
        return "{id=" + this.getId() + ", email=" + this.getEmail() + "}";
    }
}
