package com.example;


import com.example.dao.CommentDAO;
import com.example.dao.FeatureDAO;
import com.example.dao.UserDAO;
import com.example.entities.Comment;
import com.example.entities.Feature;
import com.example.entities.MoscowEnum;
import com.example.entities.User;
import com.example.util.CommentDAOFactory;
import com.example.util.FeatureDAOFactory;
import com.example.util.JPAUtil;
import com.example.util.UserDAOFactory;

import javax.persistence.EntityManager;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        CommentDAO commentDAO = CommentDAOFactory.getInstance().getCommentDAO();
        UserDAO userDAO = UserDAOFactory.getInstance().getUserDAO();
        FeatureDAO featureDAO = FeatureDAOFactory.getInstance().getUserDAO();

        Comment comment = new Comment();
        User user = new User();
        Feature feature = new Feature();

        user.setEmail("email_1");
        comment.setText("comment text");
        feature.setDescription("feature description");
        feature.setEstimate(2);

        EntityManager em = null;

        try {
            em = JPAUtil.getEntityManager();
            em.getTransaction().begin();

            user = userDAO.save(user);
            comment = commentDAO.save(comment);
            feature = featureDAO.save(feature);

            comment.setUser(user);
            feature.setComment(comment);
            feature.setMoscow(MoscowEnum.MOSCOW1);

            em.getTransaction().commit();

            List<Comment> comments = commentDAO.getAll();
            System.out.print("[INFO] ");
            comments.forEach(System.out::println);
        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }


//    Employee employee = new Employee("Alex", "Bob", 123456.);
//    employeeDAOFactory.getEmployeeDAO().create(employee);
//
//    Employee employee2 = employeeDAOFactory.getEmployeeDAO().getOne(employee.getId());
//    System.out.println(employee2);
//
//    HibernateUtil.getSessionFactory().close();
    }
}
