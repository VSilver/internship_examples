package com.example.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

public interface BaseDAO <T, K extends Serializable> {


    EntityManager getEntityManager();
    T save(T entity);

    T getOne(K id);

    List<T> getAll();

    void delete(K id);

    void delete(T entity);
}
