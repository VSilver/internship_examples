package com.example.dao;

import com.example.entities.Feature;

public interface FeatureDAO extends BaseDAO<Feature, Long> {

}
