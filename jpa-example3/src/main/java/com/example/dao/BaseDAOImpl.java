package com.example.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.example.util.JPAUtil;

public abstract class BaseDAOImpl<T, K extends Serializable> implements BaseDAO<T, K> {

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    BaseDAOImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class<T>) pt.getActualTypeArguments()[0];
    }

    public EntityManager getEntityManager() {
        return JPAUtil.getEntityManager();
    }

    public T save(T entity) {
        EntityManager em = getEntityManager();
        em.persist(entity);
        return entity;
    }

    public T getOne(Long id) {
        EntityManager em = getEntityManager();
        T entity = null;

        entity = em.find(type, id);

        return entity;
    }

    public List<T> getAll() {
        EntityManager em = getEntityManager();
        List<T> entities = null;

        CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(type);
        Root<T> root = cq.from(type);
        cq = cq.select(root);
        TypedQuery<T> q = em.createQuery(cq);
        entities = q.getResultList();

        return entities;
    }

    public void delete(Long id) {
        T entity = getOne(id);
        if (entity != null) {
            delete(entity);
        }
    }

    public void delete(T entity) {
        EntityManager em = getEntityManager();
        em.remove(entity);
    }
}
