package com.example.dao;

import com.example.entities.Attachment;

public interface AttachmentDAO extends BaseDAO<Attachment, Long> {

}
