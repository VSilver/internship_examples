package com.example.dao;

import com.example.entities.Comment;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Date;
import java.util.List;

public class CommentDAOImpl extends BaseDAOImpl<Comment, Long> implements CommentDAO{

    @Override
    public List<Comment> getAll() {
        EntityManager em = getEntityManager();
        List<Comment> entities = null;

        CriteriaQuery<Comment> cq = em.getCriteriaBuilder().createQuery(Comment.class);
        Root<Comment> root = cq.from(Comment.class);
        cq.select(root);
        TypedQuery<Comment> q = em.createQuery(cq);
        entities = q.getResultList();

        return entities;
    }

    @Override
    public Comment save(Comment comment) {
        EntityManager em = getEntityManager();
        comment.setDateCreated(new Date(System.currentTimeMillis()));
        em.persist(comment);
        return comment;
    }
}
