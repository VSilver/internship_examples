package com.example.dao;

import com.example.entities.Attachment;

import javax.persistence.EntityManager;
import java.sql.Date;

public class AttachmentDAOImpl extends BaseDAOImpl<Attachment, Long> implements AttachmentDAO {
    @Override
    public Attachment save(Attachment attachment) {
        EntityManager em = getEntityManager();
        attachment.setDateUploaded(new Date(System.currentTimeMillis()));
        em.persist(attachment);
        return attachment;
    }
}
