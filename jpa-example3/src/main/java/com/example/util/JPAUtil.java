package com.example.util;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class JPAUtil {
    private static final EntityManager entityManager;

    static {
        try {
            entityManager = Persistence.createEntityManagerFactory("jpa-example").createEntityManager();
        } catch (Throwable ex) {
            System.err.println("Initial EntityManager creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }
}
