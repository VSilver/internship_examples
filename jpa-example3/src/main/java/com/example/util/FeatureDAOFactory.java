package com.example.util;

import com.example.dao.FeatureDAO;
import com.example.dao.FeatureDAOImpl;

public class FeatureDAOFactory {
    private static FeatureDAOFactory instance;
    private static FeatureDAO featureDAO;

    private FeatureDAOFactory() {}

    public static FeatureDAOFactory getInstance() {
        if (instance == null) {
            synchronized (FeatureDAO.class) {
                if (instance == null) {
                    instance = new FeatureDAOFactory();
                }
            }
        }
        return instance;
    }

    public synchronized FeatureDAO getUserDAO() {
        if (featureDAO == null) {
            synchronized (this) {
                if (instance == null) {
                    featureDAO = new FeatureDAOImpl();
                }
            }
        }
        return featureDAO;
    }
}
