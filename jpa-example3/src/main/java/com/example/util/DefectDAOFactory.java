package com.example.util;

import com.example.dao.DefectDAO;
import com.example.dao.DefectDAOImpl;

public class DefectDAOFactory {
    private static DefectDAOFactory instance;
    private static DefectDAO defectDAO;

    private DefectDAOFactory() {}

    public static synchronized DefectDAOFactory getInstance() {
        if (instance == null) {
            instance = new DefectDAOFactory();
        }
        return instance;
    }

    public static synchronized DefectDAO getUserDAO() {
        if (defectDAO == null) {
            defectDAO = new DefectDAOImpl();
        }
        return defectDAO;
    }
}
