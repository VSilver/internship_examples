package com.example.dao;

import com.example.entities.Comment;
import com.example.entities.User;
import com.example.util.CommentDAOFactory;
import com.example.util.UserDAOFactory;

import static org.junit.Assert.*;
import org.junit.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class CommentDAOImplTest{
    private CommentDAO commentDAO = CommentDAOFactory.getInstance().getCommentDAO();

    @Test
    public void testSave() {
        Comment comment = new Comment();
        comment.setText("test comment text");
        comment.setDateCreated(new Date(System.currentTimeMillis()));

        Comment comment1 = commentDAO.save(comment);

        assertNotNull(comment1);
        assertNotNull(commentDAO.getOne(comment.getId()));
        assertNotNull(commentDAO.getOne(comment1.getId()));
        assertEquals(comment, comment1);
        assertEquals(comment.getDateCreated().toString(), (new Date(System.currentTimeMillis())).toString());

        System.out.println(comment);

        Comment comment2 = commentDAO.save(comment);
        assertEquals(comment2, comment1);

        System.out.println(comment2);
    }


    @Test
    public void testGetOne() {
        Comment comment = new Comment();
        Comment comment1 = commentDAO.save(comment);
        Comment comment2 = commentDAO.getOne(comment.getId());

        assertNotNull(comment2);
        assertEquals(comment1, comment);
        assertEquals(comment, comment2);
        assertEquals(comment1, comment2);
    }

//    @Test
//    public void testGetAll() {
//        List<Comment> commentList = new ArrayList<Comment>();
//
//        for (int i = 0; i < 3; i++) {
//            Comment c = new Comment();
//            c.setText("test " + i);
//            commentList.add(c);
//            commentDAO.save(c);
//        }
//
//        List<Comment> comments = commentDAO.getAll();
//        System.out.println(comments.isEmpty());
//        assertFalse(comments.isEmpty());
//        assertTrue(comments.size() == 3);
//
//        commentList.forEach(System.out::println);
//    }

    @Test
    public void testDeleteById() {
        Comment c = new Comment();
        c.setText("test text");

        commentDAO.save(c);

        Long id = c.getId();
        System.out.println(id);

        commentDAO.delete(id);

        assertNull(commentDAO.getOne(id));
    }

    @Test
    public void testDelete() {
        Comment c = new Comment();
        c.setText("test text");

        commentDAO.save(c);

        Long id = c.getId();
        System.out.println(id);

        commentDAO.delete(c);

        assertNull(commentDAO.getOne(id));
    }

    @Test
    public void testCascadeUserSave() {
        UserDAO userDAO = UserDAOFactory.getInstance().getUserDAO();
        Comment comment = new Comment();
        User user = new User();

        user.setEmail("email_1");
        comment.setText("comment text");
        comment.setUser(user);

        comment = commentDAO.save(comment);
        assertNotNull(userDAO.getOne(user.getId()));

        System.out.println(comment);
        System.out.println(user);
    }
}
