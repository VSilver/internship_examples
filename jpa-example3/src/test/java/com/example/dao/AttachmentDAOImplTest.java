package com.example.dao;


import static org.junit.Assert.*;

import com.example.entities.Attachment;
import com.example.entities.User;
import com.example.util.AttachmentDAOFactory;
import com.example.util.UserDAOFactory;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class AttachmentDAOImplTest {
    private AttachmentDAO attachmentDAO = AttachmentDAOFactory.getInstance().getAttachmentDAO();

    @Test
    public void testSave() {
        Attachment attachment = new Attachment();
        attachmentDAO.save(attachment);
        attachment.setDateUploaded(new Date(System.currentTimeMillis()));
        attachment.setData(new Byte[01010101]);
        attachment.setFileName("Test file name");

        assertNotNull(attachment.getId());
        assertEquals(attachment.getDateUploaded().toString(), (new Date(System.currentTimeMillis())).toString());
        assertNotNull(attachment.getData());
        assertEquals(attachment.getFileName(), new String("Test file name"));
    }


    @Test
    public void testGetOne() {
        Attachment attachment = new Attachment();
        attachmentDAO.save(attachment);
        attachment.setDateUploaded(new Date(System.currentTimeMillis()));
        attachment.setData(new Byte[01010101]);
        attachment.setFileName("Test file name");

        assertNotNull(attachmentDAO.getOne(attachment.getId()));
        assertEquals(attachment, attachmentDAO.getOne(attachment.getId()));
    }

    @Test
    @Ignore
    public void testGetAll() {
        List<Attachment> attachmentList = new ArrayList<Attachment>();

        for (int i = 0; i < 3; i++) {
            Attachment a = new Attachment();
            a.setFileName("test file name" + i);
            attachmentList.add(a);
            attachmentDAO.save(a);
        }

        List<Attachment> attachments = attachmentDAO.getAll();
        System.out.println(attachments.isEmpty());
        assertFalse(attachments.isEmpty());
        assertTrue(attachments.size() == 3);

        attachmentList.forEach(System.out::println);
    }

    @Test
    public void testDeleteById() {
        Attachment attachment = new Attachment();
        attachmentDAO.save(attachment);

        Long id = attachment.getId();
        assertNotNull(id);

        attachmentDAO.delete(id);
        assertNull(attachmentDAO.getOne(id));
    }

    @Test
    public void testDelete() {
        Attachment attachment = new Attachment();
        attachmentDAO.save(attachment);

        Long id = attachment.getId();
        assertNotNull(id);

        attachmentDAO.delete(attachment);
        assertNull(attachmentDAO.getOne(id));
    }

    @Test
    public void testCascadeUserSave() {
        UserDAO userDAO = UserDAOFactory.getInstance().getUserDAO();
        Attachment attachment = new Attachment();
        User user = new User();

        user.setEmail("email_1");
        attachment.setFileName("attachment text");
        attachment.setUser(user);

        attachment = attachmentDAO.save(attachment);
        assertNotNull(userDAO.getOne(user.getId()));

        System.out.println(attachment);
        System.out.println(user);
    }
}
