package net.news;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by vturcanu on 3/13/2017.
 */
public interface Hello extends Remote {
    String sayHello() throws RemoteException;
}
