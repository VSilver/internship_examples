package net.news;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by vturcanu on 3/13/2017.
 */
public class Client {

    private static final String ADRIANS_IP = "172.16.46.174";
    private static final String LOCALHOST = "localhost";

    public static void main(String[] args) {
        try {
            Registry registry = LocateRegistry.getRegistry(LOCALHOST, 5678);
            Hello myHello = (Hello) registry.lookup("hello");
            System.out.println(myHello.sayHello());
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }
}
