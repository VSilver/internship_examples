package net.news;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by vturcanu on 3/13/2017.
 */
public class Server {
    public static void main(String[] args) {
        try {
            Hello serverHello = new HelloImpl();
            Registry registry = LocateRegistry.createRegistry(5678);
            registry.rebind("hello", serverHello);
            System.out.println("Server is running. Binding completed.");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
