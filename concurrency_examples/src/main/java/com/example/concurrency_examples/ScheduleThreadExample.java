package com.example.concurrency_examples;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduleThreadExample {
	public static void main(String[] args) throws InterruptedException,
			ExecutionException {
		ScheduledExecutorService service = null;
			service = Executors.newSingleThreadScheduledExecutor();

			Runnable task1 = () -> System.out.println("Hello from task1");
			Callable<String> task2 = () -> {
				return "Hello from task2";
			};

			Future<?> result1 = service.schedule(task1, 2, TimeUnit.SECONDS);
			Future<String> result2 = service.schedule(task2, 8, TimeUnit.SECONDS);

			System.out.println(result2.get());
			
			service.scheduleAtFixedRate(() -> System.out.println("2"), 0, 1, TimeUnit.SECONDS);

		if (service != null) {
			service.awaitTermination(30, TimeUnit.SECONDS);
			// Check whether all tasks are finished
			if (service.isTerminated())
				System.out.println("All tasks finished");
			else
				System.out.println("At least one task is still running");
		}
	}
}
