package com.example.client;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 * Created by vturcanu on 3/17/2017.
 */
public class Sender {
    private static final String connectionFactoryLookup = "JMSDEMO";
    private static final String JNDIQueue = "JMSDEMO.Q1";

    private Context context;
    private Session session;
    private Connection connection;
    private ConnectionFactory connectionFactory;
    private String destinationLookup;
    private Destination queue1;

    public Sender(Context context) {
        this.context = context;
    }

    public void sendMessage() throws JMSException {
        MessageProducer producer = session.createProducer(queue1);
        String message = "Buy some actions";

        System.out.println("\nSending message" + message + " to " + queue1);
        TextMessage outMessage = session.createTextMessage(message);
        producer.send(outMessage);
        session.commit();
        producer.close();
    }

    public void start() {
        try {
            System.out.println("Lookup connection factory" + connectionFactoryLookup);
            connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryLookup);

            System.out.println("Create and start the connection");
            connection = connectionFactory.createConnection();
            connection.start();

            System.out.println("Create the session");
            boolean transacted = true;
            session = connection.createSession(transacted, Session.AUTO_ACKNOWLEDGE);

            destinationLookup = JNDIQueue;
            System.out.println("Lookup destination" + destinationLookup);
            queue1 = (Destination) context.lookup(destinationLookup);

        } catch (JMSException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        try {
            if (session != null) {
                System.out.println("Closing sender session");
                session.close();
            }
            if (connection != null) {
                System.out.println("Closing sender connection");
                connection.close();
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
