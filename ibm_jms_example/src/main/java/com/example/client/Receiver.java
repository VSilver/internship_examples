package com.example.client;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 * Created by vturcanu on 3/17/2017.
 */
public class Receiver implements MessageListener {

    private static final String connectionFactoryLookup = "JMSDEMO";
    private static final String JNDIQueue = "JMSDEMO.Q1";

    private Session session;
    private Connection connection;
    private ConnectionFactory connectionFactory;
    private String destinationLookup;
    private Context context;
    private MessageConsumer consumer;

    public Receiver(Context context) {
        this.context = context;
    }

    public void onMessage(Message message) {
        try {
            System.out.println("Got the message from the JMSDEMO.Q1 " +
                    message.getBody(String.class));

            System.out.println("\n=== Here is what toString() on the " +
                    "message prints \n" + message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        try {
            System.out.println("Lookup connection factory" + connectionFactoryLookup);
            connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryLookup);

            System.out.println("Create and start the connection");
            connection = connectionFactory.createConnection();
            connection.start();

            System.out.println("Create the session");
            boolean transacted = true;
            session = connection.createSession(transacted, Session.AUTO_ACKNOWLEDGE);

            destinationLookup = JNDIQueue;
            System.out.println("Lookup destination" + destinationLookup);
            Destination queue1 = (Destination) context.lookup(destinationLookup);

            consumer = session.createConsumer(queue1);
            consumer.setMessageListener(this);
            System.out.println("Listening to the JMSDEMO.Q1 ----------------------");
            Thread.sleep(50000);
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        try {
            if (session != null) {
                System.out.println("Closing receiver session");
                session.close();
            }
            if (connection != null) {
                System.out.println("Closing receiver connection");
                connection.close();
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
