package com.example.client;

import javax.jms.*;

/**
 * Created by vturcanu on 3/17/2017.
 */
public class SecondSender {

    ConnectionFactory connectionFactory;
    Connection connection;
    Session session;
    Destination destination;
    MessageProducer messageProducer;

    public SecondSender(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public void startUp() {
        try {
            connection = connectionFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            destination = session.createQueue("queue:///JMSDEMO.Q1");
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage() {

    }
}
