package com.example.main;

import com.example.client.Receiver;
import com.example.client.Sender;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

/**
 * Created by vturcanu on 3/16/2017.
 */
public class Main {

    private static final String connectionFactoryLookup = "JMSDEMO";
    private static final String JNDIQueue = "JMSDEMO.Q1";

    private static Session session;
    private static Connection connection;
    private static ConnectionFactory connectionFactory;
    private static String destinationLookup;

    public static void main(String[] args) throws NamingException {
        String initContextFactory1 = "com.ibm.mq.jms.context.WMQInitialContextFactory";
        String initContextFactory = "com.sun.jndi.fscontext.RefFSContextFactory";

        System.out.println("Lookup initial context");
        Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY, initContextFactory);
        env.put(Context.PROVIDER_URL, "localhost:1414/SYSTEM.DEF.SVRCONN");
        Context context = new InitialContext(env);

        Receiver receiver = new Receiver(context);
        Sender sender = new Sender(context);

        try {
            sender.start();
            receiver.start();

            sender.sendMessage();

            sender.stop();
            receiver.stop();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
