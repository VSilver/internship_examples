package com.example.main;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;

import javax.jms.*;
import java.util.Properties;

/**
 * Created by vturcanu on 3/17/2017.
 */
public class SecondMain {

    public static void main(String[] args) {

        try {
            JmsFactoryFactory jmsFactoryFactory = JmsFactoryFactory.getInstance(WMQConstants.WMQ_PROVIDER);
            JmsConnectionFactory connectionFactory = jmsFactoryFactory.createConnectionFactory();

            connectionFactory.put(WMQConstants.WMQ_HOST_NAME, "localhost");
            connectionFactory.put(WMQConstants.WMQ_PORT, 1414);
            connectionFactory.put(WMQConstants.WMQ_CHANNEL, "SYSTEM.DEF.SVRCONN");
            connectionFactory.put(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
            connectionFactory.put(WMQConstants.WMQ_QUEUE_MANAGER, "JMSDEMO");
            connectionFactory.put(WMQConstants.WMQ_PROVIDER_VERSION, "8.0.0.0");



        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
