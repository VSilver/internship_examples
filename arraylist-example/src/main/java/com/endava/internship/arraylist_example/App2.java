package com.endava.internship.arraylist_example;

import java.util.ArrayList;
import java.util.List;

public class App2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Object> list = new ArrayList<>();
		
		list.add("some string");
		list.add(1254);
		
		list.forEach(System.out::println);
	}

}
