package com.endava.internship.arraylist_example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        List<String> birds = new ArrayList<String>();
        
        birds.add("cardinal");
        birds.add("hawk");
        birds.add("hawk");
        System.out.println(birds.remove("cardinal"));
        System.out.println(birds.remove("cardinal"));
        System.out.println(birds.remove(0));
        System.out.println(birds.set(0, "pigeon"));
        System.out.println(birds.toString());
        
        birds.clear();
        System.out.println(birds.isEmpty());
        
        List<String> list = Arrays.asList("zfirst", "second");
        Collections.sort(list);
        System.out.println(list);
        
        String s1 = "abc";
        String s2 = new String(s1);
        System.out.println(s2 == "abc");
        
        Supplier<Integer> s = () -> 1;
        
        StringBuilder strBuilder = new StringBuilder("");
        StringBuilder strBuilder2 = new StringBuilder();
        
        System.out.println(strBuilder);
        System.out.println(strBuilder2);
    }
}
