package com.example.main;


public interface MyReduce <T, BiFunction> {
    public T reduce(T identity, BiFunction func);
}
