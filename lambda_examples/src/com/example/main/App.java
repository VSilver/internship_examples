package com.example.main;


import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {list.add(i);}

        BiFunction<Integer, Integer, Integer> sum = (a, b) -> a + b;

        MyReduce<Integer, BiFunction<Integer, Integer, Integer>> myReduce;

        System.out.println(Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9)
                .reduce(0, (a, b) -> a + b));
    }

    public Integer reduce(Integer identity, BiFunction<Integer, Integer, Integer> accumulator) {
        return 0;
    }
}
