package com.example.main;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Main {

    public static void main(String[] args) {
        String str = "abc";

        Predicate<String> method1 = str::startsWith;
        Predicate<String> lambda1 = s -> str.startsWith(s);

        System.out.println(method1.test("s"));
        System.out.println(lambda1.test("a"));

        Predicate<String> lambda2 = s -> s.isEmpty();

        System.out.println(lambda2.test("a"));
        System.out.println(lambda2.test(""));

        Consumer<String> lambda3 = s -> System.out.println(s);

        lambda3.accept("This string is printed by a consumer");

        Supplier<String> lambda4 = () -> new StringBuilder().append("Supplied string").toString();
        Supplier<ArrayList<String>> lambda5 = ArrayList<String>::new;

        System.out.println(lambda4.get());

        List<String> list = lambda5.get();
        list.add("Magician");
        list.add("Assistant");
        list.add("");
        System.out.println(list);
        list.removeIf(s -> s.startsWith("A"));
        System.out.println(list);
        list.removeIf(String::isEmpty);
        System.out.println(list);

        // use merge on HashMap of Criterias

    }
}
